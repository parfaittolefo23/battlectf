# Solution babyrev

Dans ce challenge, j'ai préféré [dogbolt](https://dogbolt.org/)

Mais rassurez d'avoir choisi l'option **A CTF Challenge** pour une decompilation plus détaillée.

<img src="1.png">

Décompiler, un bout de code de checking vérifie ceci: **qpiiatRIU{Pvqp_Ugt3_UDDS_Stn_d0D!_85864r1277qu8195pqqtp6540494pr46}**


    void encrypt(char *__block,int __edflag)

    {
    char cVar1;
    int iVar2;
    size_t sVar3;
    ushort **ppuVar4;
    int local_c;
    
    sVar3 = strlen(__block);
    for (local_c = 0; local_c < (int)sVar3; local_c = local_c + 1) {
    cVar1 = __block[local_c];
    ppuVar4 = __ctype_b_loc();
    if (((*ppuVar4)[cVar1] & 0x400) != 0) {
      ppuVar4 = __ctype_b_loc();
      if (((*ppuVar4)[cVar1] & 0x200) == 0) {
        iVar2 = __edflag + cVar1 + -0x41;
        __block[local_c] = (char)iVar2 + (char)(iVar2 / 0x1a) * -0x1a + 'A';
      }
      else {
        iVar2 = __edflag + cVar1 + -0x61;
        __block[local_c] = (char)iVar2 + (char)(iVar2 / 0x1a) * -0x1a + 'a';
      }
        }
    }
    return;
    }

Lire le code pour comprendre, il s'agissait de ROT-15

**flag:** battleCTF{Agba_Fre3_FOOD_Dey_o0O!_85864c1277bf8195abbea6540494ac46}
