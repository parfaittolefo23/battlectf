# Solution SEYI

La première des chose, c'est de vérifier le type du binaire et ces caratéristique.

`file seyi`, il sagi d'un programme compilé linux ELF 64-bit.

<img src='1.png'>

Exécuté, rien 

`strings seyi`  pour voir les chaines de caratéres du binaire, on obtien un format flag: **CTF{TheH_path_toHpath_to_Hlight}**

Malheureusement, c'est pas le bon flag (la casse)

Alors j'ai opté pour les decompileures en ligne. Le cool que j'adore est [ogbolt.org](https://dogbolt.org/)

Décompilé, on obtient le flag 

**flag:** battleCTF{The_path_to_light}
