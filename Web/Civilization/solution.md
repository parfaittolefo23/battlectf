# Solution Civilization

Une visite du site web est la toutes première des choses. Il s'agit d'un _landing page_.
En bas de la page, il est mis **Don't forget your ?source**. Directement le **?source** fait pensé à un paramettre recpéré par la methode GET de php.

Allons voir:

<img src='1.png'>

Ici, le code source de la partie qui nous founira le flag. 

Pour avoir le flag, nous devons entrer le mot _africacradlecivilization_ or la partie `preg_replace("/$cigar/",'',$input) === $cigar` supprime les occurence de  _africacradlecivilization_ avant de vérifier si le reste est égal à _africacradlecivilization_. 

Alors, c'est simple, il faut inserer au milieu de _africacradlecivilization_ un autre _africacradlecivilization_ qui sera supprimé pour faire combiner les deux autre partie et le flag est donné:

Avec la requette: `?source=blabla&ami=africacradlafricacradlecivilizationecivilization`

**flag:**  battleCTF{pr3gr4plAcebyp455_0x0x0x0x}
