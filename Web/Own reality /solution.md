# Solution Own reality

Un fuzing du site avec  `dirsearch -u http://chall.battlectf.online:8082/` nous liste des repos suivant dont un .git

<img src='/Web/Own reality/2.png'>

Avec _git-dumper_ on peut avoir le .git: `git-dumper http://chall.battlectf.online:8082/.git/ dumpWeb`

L'objectif des de vérifier les log pour voir si quelques chose n'avait pas changé sur le projet.

Voici la démarche:

1. `git log`

Cette commande liste des logs, on obtient:

    commit ff092a2d7c85f81a47131b7ef303ba8ece1a8492 (HEAD -> ff092a2d7c85f81a47131b7ef303ba8ece1a8492, master, 2, 1)
    Author: w31rdr4v3n <w31rd.rav3n@gmail.com>
    Date:   Wed Apr 26 04:58:02 2023 -0400

    Ok

    commit a1346a3abab8f97748e5480b61eb6824d4692f44
    Author: w31rdr4v3n <w31rd.rav3n@gmail.com>
    Date:   Wed Apr 26 04:56:14 2023 -0400

    Sability

2. `git checkout ff092a2d7c85f81a47131b7ef303ba8ece1a8492 -b flag`

Cette commande permet de créer autre branche sur le présent commit

3. `git switch -c flag`

Allez sur la branche

3. `git diff ff092a2d7c85f81a47131b7ef303ba8ece1a8492`

Pour voir les différence entre les commits. Vous verrez quelque chose comme du morse, mais ce n'est pas le cas.

    .__..._..__...._.___._...___._...__.__...__.._._._....__._._._..._...__..____.__._._._._.__.___..__._.__.__.___..__.____.___.___.__.___.._._____.__..._..__._.._.___._...___..__._._____..__..__..___.....__._...__.._._.__.._._.__...._..__._....___.._.__..._...__._....__..._..__.___.__.._._.__.._._..__.._..__..__..__..__...__._._.__...._..__..._..__..__.__..__..__..._..__.._...__...__.__...__.__...._..__.__..__..__...__..__..__.._...__.___._____._

4. Remplacer juste les points(.) par 0 et les underscore(_) par 1. et decodez from binary:


**flag:** battleCTF{Unknown_bits_384eea49b417ee2ff5a13fbdcca6f327}


