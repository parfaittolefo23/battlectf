Il s'agit ici d'un landing page. Le fuzzing ne revèle aucun dossier ou fichier.
Un scanage avec **nikto** soit `nikto -h http://chall.battlectf.online:8086/` revèle le secret suivant:

    Python/3.8.17 appears to be outdated (current is at least 3.9.6).

Une idée ???

Ça fait pensée systématique au **SSIT liée au flask de python** 

Pour confimer ceci, allons voir la sortie de `whatweb http://chall.battlectf.online:8086/`

<img src='2.png'>

Alors, quel paramèttre 
