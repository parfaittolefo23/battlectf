# Title: Cobalt Injection 2

**pts:** 400

AM1 has improved its country guessing game, eliminating all distractions and rejecting the odd request.

http://chall.battlectf.online:8086/

Author: 0xRAVEN

<img src='1.png'>
