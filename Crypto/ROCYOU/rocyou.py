from pwn import *

p = remote('chall.battlectf.online', 20001)
#context. log_level = 'debug'

enc = bytes.fromhex(p.recvline().decode().strip())[:-1]
print(enc)
#v = enc
flag = b''
while flag[-1:] != b'}':
    fl= len(flag)
    for i in range(0x20,0x7f):
        p.sendlineafter(b'>', flag + bytes([i]))
        v = bytes. fromhex(p.recvline().decode().strip())
        if v[fl] == enc[fl]:
            flag += bytes([i])
            print (flag)

            break
