# Solution ROCYOU

A lire le code qui nous est  donné, il s'agit d'un chiffrement RSA.

Vous pouvez jetter un coup d'eoil [ici](https://en.wikipedia.org/wiki/RSA_(cryptosystem)) pour voir comment ça mache, le chiffrement RSA.

Si vous avez déja pris connaissance du focntionnement de RSA, vous pouvez retenir qu'il utilise essentiellement de éléments capitaux:

**La clée publique:** (n,e) où n=p*q

**La clé privée:** (n,d)

Bref, l'objectif c'est d'avoir le flag. Nous pouvons essayer de bruteforcer avec [dcode.fr](https://www.dcode.fr/chiffre-rsa)

<img src="2.png">

Malheureusement, null 

J'ai pensé factoriser le _n_ sur [factordb.com](http://factordb.com/), mais rien de ouf...

Après quelque recherche, je suis tombé sur la vulnérabilté [ROCA](https://en.wikipedia.org/wiki/ROCA_vulnerability) qui permet d'extraire la clé privé à partir de la clé publique. 

Je vous fais fi des démarche intermédiaires...

**Neca** ([téléchargé ici ](https://gitlab.com/parfaittolefo23/astuces-et-write-up-ctf/-/blob/main/Tools/neca)) est le tool approprié que j'ai trouvé.

<img src="1.png">

P=127801155916875524149457561567678575565270601000365665873572024750823913157383 q=113917064871970833547038329106470040388258358281464605006613652518914797349747


**Bingo !!!**

<img src="3.png">

**flag:** _battleCTF{ROCA_shork_me_0x0x0x}_ 
