# Solution Blind

La description du challenge nous donne une idée !!! **Base**

Les caractères présents m'ont fait pensé à la **base85**

Mais rien, heureusement nous une option dite **magic** sur  [CyberChef](https://gchq.github.io/CyberChef/)

<img src='1.png'>

Décodé, on obtien du code **braille**, le langage des sous.

<img src='2.png'>

flag: **battleCTF{WHY_D0N7_811ND_P30P13_5KYD1V3_N0_8R41113_1N57RUC710N5}**
