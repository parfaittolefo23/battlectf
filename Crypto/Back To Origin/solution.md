# Solution

Nous avons une image avec des stickers on dirait. N'ayant pas d'idée, faisons une recherche inversée de l'image avec google lens.

<img src="Metaphysics.png">

On tombe sur la plateforme [discoveringegypt.com](https://discoveringegypt.com/hieroglyphic-typewriter/) qui permet sde faire du décodage hiéroglyphique (hiéroglyphe égyptien).
<img src="2.png">

Décodé, on onbtient le message: **AFRICAFAMILY**

flag: battleCTF{AFRICAFAMILY}
