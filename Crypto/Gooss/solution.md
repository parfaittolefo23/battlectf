# Solution Gooss

Il est fourni un script python qui essai de chiffrer d'une manière ou d'une autre le flag.

Comment le script fonctionne ?

1. Il Génère dans un premier temps des nombres de façcons alléatoire **(a,b,c,d,e)**

2. Il parcourt toutes les lettres du flag en éffectuant l'oppération suivante:

`res = (2*a*pow(ord(x),4)+b*pow(ord(x),3)+c*pow(ord(x),2)+d*ord(x)+e)`

**Note:** 

`pow(a,b)` = a^b
`ord(a)`= valeur ascii de `a` = 97

Connaissance au moins les 5 premiers caratères mise en jeux et les résultats, nous pouvons utiliser Elimination de Gauss (Pivot)

pour trouver les valeurs des variables.

[dcode.fr](https://www.dcode.fr/solveur-equation) ou [matrixcalc.org](https://matrixcalc.org/fr/slu.html)fait bien le job.

Le sytème d'équation serait:

    184473632*a+941192*b+9604*c+98*d+1*e=1245115057305148164
    177058562*a+912673*b+9409*c+97*d+1*e=1195140205147730541
    362127872*a+1560896*b+13456*c+116*d+1*e=2441940832124642988
    362127872*a+1560896*b+13456*c+116*d+1*e=2441940832124642988
    272097792*a+1259712*b+11664*c+108*d+1*e=1835524676869638124
    208120802*a+1030301*b+10201*c+101*d+1*e=1404473868033353193


Et la Solution est:

    a=6709636436

    b=7748795321

    c=7386429784

    d=62359624

    e=5008041292

# Voici le script pour trouver le flag

    flag = []
    a=6709636436

    b=7748795321

    c=7386429784

    d=62359624

    e=5008041292

    tab=[1245115057305148164, 1195140205147730541, 2441940832124642988, 2441940832124642988, 1835524676869638124, 1404473868033353193, 272777109172255911, 672752034376118188, 324890781330979572, 3086023531811583439, 475309634185807521, 1195140205147730541, 2441940832124642988, 1578661367846445708, 2358921859155462327, 1099718459319293547, 773945458916291731, 78288818574073053, 2441940832124642988, 1578661367846445708, 1099718459319293547, 343816904985468003, 1195140205147730541, 2527132076695959961, 2358921859155462327, 2358921859155462327, 1099718459319293547, 72109063929756364, 2796116718132693772, 72109063929756364, 2796116718132693772, 72109063929756364, 2796116718132693772, 3291439457645322417]

    def encrypt(x):
        return (2*a*pow(ord(x),4)+b*pow(ord(x),3)+c*pow(ord(x),2)+d*ord(x)+e)


    for val in tab:
        for i in range(32,126):
            if(encrypt(chr(i))==val):
                flag.append(chr(i))
                break
    print('Flag: '+''.join(flag))



**flag**: battleCTF{Maths_W1th_Gauss_0x0x0x}



